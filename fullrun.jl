begin
    # Working in the local environment
    using Pkg
    Pkg.activate(@__DIR__)
end

begin
    # Load dependencies
    using LightGraphs
    import GraphPlot
    import Compose:cm, SVG, PDF
    import Gadfly
    import Colors
    import Cairo
    import Fontconfig
    using Random
    using DifferentialEquations
    using Distributions
    import Plots
    using JLD2
    using Clustering
    using LaTeXStrings
    import PyPlot
end

using MCBB

begin
    seed = 5386748129040267798
    Random.seed!(seed)
    # Set up the parameters for the network
    N = 30 # in this case this is the number of oscillators, the system dimension is twice this value
    g = random_regular_graph(N, 3)
    E = incidence_matrix(g, oriented=true)
    drive = [isodd(i) ? +1. : -1. for i = 1:N]
    par = second_order_kuramoto_parameters(N, 0.1, 5., E, drive)
    T = 2000.
end


begin
    # These functions define the distributions from which initial conditions and
    # parameters are drawn, as well as which parameter to vary.
    ic_ranges = ()->rand(Uniform(-pi,pi))
    if length(ARGS) == 0
        N_ics = 500
    else
        N_ics = parse(Int, ARGS[1])
    end
    K_range = (i)-> 10. * i / N_ics
    par_vars = (:coupling, K_range)
    tail_frac = 0.9
    state_filter = collect(N+1:2*N)
end

# evaluation of the solutions, we set a state_filter to only analyse the
# frequencies and not the phases.
function eval_ode_run_inertia(sol, i)
    state_filter = collect(N+1:2*N)
    eval_funcs = [MCBB.mean, MCBB.std]
    global_eval_funcs = []
    eval_ode_run(sol, i, state_filter, eval_funcs, global_eval_funcs)
end

begin
    knp = ODEProblem(second_order_kuramoto, zeros(2*N), (0.,T), par)
    knp_mcp = DEMCBBProblem(knp, ic_ranges, N_ics, par, par_vars, eval_ode_run_inertia, tail_frac)
end

println("Beginning simulation")
knp_sol = solve(knp_mcp)
D_k = distance_matrix(knp_sol, knp_mcp, [1.,0.,1.])
println("Simulation done")

begin
    fdist = k_dist(D_k,4)
    # Plots.plot(collect(1:N_ics),fdist[1:N_ics])
    # Plots.plot(collect(1000:N_ics),fdist[1000:N_ics])
end

println("Heuristic for epsilon: $(median(KNN_dist_relative(D_k)))")

begin    # The main bifurcation diagram, this sometimes needs to be run twice or it crashes...
    db_eps = 5
    min_c_size = N_ics/200
    if min_c_size < 5
        min_c_size = 5
    end
    db_res = dbscan(D_k,db_eps,round(Int, min_c_size))
    # Only differentiate clusters that have at least 0.5% of the overall probability
    cluster_members = cluster_membership(knp_mcp, db_res, 0.25, 0.1);
    sort!(cluster_members)
    println(size(cluster_members))
    # Plots.pgfplots()
    plt = Plots.plot(cluster_members, linecolor="white", linewidth=0.1, fillalpha=0.6, xlabel=L"Coupling $K$") # , min_members = round(Int,N_ics/200/5)
    Plots.savefig(plt, joinpath("pictures","kur-membership.pdf"))
end

N_clusters = size(cluster_members)[2]

cmr = cluster_measures(knp_mcp, knp_sol, db_res, 0.25, 0.1);

for i in 1:N_clusters
    plt = Plots.plot(cmr, 1, i)
    Plots.savefig(plt, joinpath("pictures","kur-cluster$i-dim-means.pdf"))
end

for i in 1:N_clusters
    plt = Plots.plot(cmr, 2, i)
    Plots.savefig(plt, joinpath("pictures","kur-cluster$i-dim-vars.pdf"))
end

# m = get_trajectory(knp_mcp,knp_sol, db_res, 3 ,only_sol=true)
# Plots.plot(m, vars=state_filter)

Plots.pyplot()

# Plot histograms for means
shists = cluster_measures_sliding_histograms(knp_mcp, knp_sol, db_res, 1, 0.25, 0.1);

for i in 1:N_clusters
    plt = Plots.plot(shists, i)
    Plots.savefig(plt, joinpath("pictures","kur-cluster$i-histo.pdf"))
end

# Plot histograms for vars
shists = cluster_measures_sliding_histograms(knp_mcp, knp_sol, db_res, 2, 0.25, 0.1);

for i in 1:N_clusters
    plt = Plots.plot(shists, i)
    Plots.savefig(plt, joinpath("pictures","kur-cluster$i-histo-var.pdf"))
end


#Plot the graphs with the means of the frequencies:

cmeans = cluster_means(knp_sol, db_res)

fixed_layout = GraphPlot.spring_layout(g)
layout_func = (x) -> fixed_layout

cg = Plots.cgrad(:RdBu, range(-10., 10., length=100))

net_plot = GraphPlot.gplot(g, nodefillc=[d > 0. ? "blue" : "red" for d in drive], layout=layout_func)
Gadfly.draw(PDF(joinpath("pictures", "networks", "kur-network-power.pdf"), 16cm, 16cm), net_plot)

for i in 1:size(cmeans)[1]
    net_plot = GraphPlot.gplot(g, nodefillc=[cg[cmean] for cmean in cmeans[i,1,:]], layout=layout_func)
    Gadfly.draw(PDF(joinpath("pictures", "networks", "kur-cluster$i-network.pdf"), 16cm, 16cm), net_plot)
end
