begin
    # Working in the local environment
    using Pkg
    Pkg.activate(@__DIR__)
end

begin
    # Load dependencies
    using MCBB
    using LightGraphs
    import PyPlot
    using Random
    using DifferentialEquations
    using Plots
    using Distributions
end

begin
    seed = 5386748129040267798
    Random.seed!(seed)
    # Set up the parameters for the network
    N = 30 # in this case this is the number of oscillators, the system dimension is twice this value
    T = 2000.
    g = random_regular_graph(N, 3)
    E = incidence_matrix(g, oriented=true)
    drive = [isodd(i) ? +1. : -1. for i = 1:N]
    par = second_order_kuramoto_parameters(N, 0.1, 5., E, drive)
end

begin
    # Some random trajectories
    ic_gen = ()->rand(Uniform(-pi,pi))
    ic = [ic_gen() for i in 1:2*N]

    knp = ODEProblem(second_order_kuramoto, ic, (0.,T/4.), par)
    sol = solve(knp)
    freq_plot = plot(sol, legend = false, vars=N+1:2*N)
    phase_plot = plot(sol, legend = false, vars=1:N+1)
    savefig(freq_plot, joinpath("pictures","example_trajectory_freq.svg"))
    savefig(phase_plot, joinpath("pictures","example_trajectory_phase.svg"))
end

begin
    # There are many potential asymptotic states which we can sample and plot.
    # We are only interested in the frequencies here.
    N_sample = 16
    state_filter = collect(N+1:2*N)

    asymptotes = Array{Float64}(undef, N_sample,N)
    for i in 1:N_sample
        ic = [ic_gen() for i in 1:2*N]

        test_problem = ODEProblem(second_order_kuramoto, ic, (0.,T), par)
        tsol = solve(test_problem)
        asymptotes[i, :] .= tsol[end][state_filter]
    end
end

begin
    plt_asymp = scatter(1:N, asymptotes[1, :], alpha=0.5, legend=false)
    for i in 2:N_sample
        scatter!(plt_asymp, 1:N, asymptotes[i, :], alpha=0.5)
    end
    savefig(plt_asymp, joinpath("pictures","asymptote_scatter.svg"))
end

begin
    # To reduce noise in the asymptotic frequencies we can average them. We use
    # the internal interoplation capabilities of the DiffEq solution object to
    # obtain a regular sample.
    tail_frac = 0.9
    asymptotes2 = zeros(N_sample,N)
    for i in 1:N_sample
        ic = [ic_gen() for i in 1:2*N]

        test_problem = ODEProblem(second_order_kuramoto, ic, (0.,2000.), par)
        tsol = solve(test_problem)
        for t in 0.9*T:T
            asymptotes2[i, :] .+= tsol(t)[state_filter]
        end
    end
    asymptotes2 ./= length(0.9*T:T)
end


begin
    plt_asymp2 = scatter(1:N, asymptotes2[1, :], alpha=0.5, legend=false)
    for i in 2:N_sample
        scatter!(plt_asymp2, 1:N, asymptotes2[i, :], alpha=0.5)
    end
    savefig(plt_asymp2, joinpath("pictures","asymptote2_scatter.svg"))
end

begin
    # We can also plot them side by side. ToDo: Rewrite this in Plots.
    fig = PyPlot.figure(figsize=(10,10))
    csubplot = 1
    for i in 1:4
        for j in 1:4
            global csubplot
            PyPlot.subplot(4,4,csubplot)
            PyPlot.scatter(1:N, asymptotes2[csubplot, :])
            PyPlot.ylim(-11,11)
            csubplot += 1
        end
    end
    PyPlot.savefig(joinpath("pictures","asymptote2_grid.svg"))
end
