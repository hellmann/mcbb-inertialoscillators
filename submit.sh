#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=mcbb-inertial
#SBATCH --account=coen
#SBATCH --output=mcbb-inertial-%j-%N.out
#SBATCH --error=mcbb-inertial-%j-%N.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mail-type=END
#SBATCH --exclusive

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "------------------------------------------------------------"

module load julia/1.1.0
module load hpc/2015
julia 1-simulate.jl 25000
julia 2-analyze.jl 25000
